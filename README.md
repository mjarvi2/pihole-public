# pihole-public

Public version of my pihole repo, to easily spin up piholes and 
apply updates when they are available. Spin up a new ubuntu box, 
install git, clone this repo, and  run the bootstrap.sh script,
and let automation handle the rest.

## Suggested Usage

The commands within the script are meant to run on a fresh ubuntu machine

Fork this repo, add your own lists, or submit a MR to this repo
to add to the list of blacklists.
