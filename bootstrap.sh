#!/bin/bash
ipaddr=$(hostname -I | awk '{print $1}')
dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
sedsafedir=$(printf '%s\n' "$dir" | sed -e 's/[]\/$*.^[]/\\&/g');
printf "\nPiHole Install Script. HAJIME!\n\n"
apt update
apt upgrade -y
apt install curl -y
apt purge network-manager-config-connectivity-ubuntu -y
systemctl stop systemd.resolved
systemctl disable systemd.resolved
apt install docker.io -y
sed -i "s/SCRIPT_FILE/$sedsafedir/g" $dir/blacklist.service
cp $dir/blacklist.service /etc/init.d/blacklist
chmod 755 /etc/init.d/blacklist
# Restore the original file to prevent blacklist syncing issues
git -C $dir restore blacklist.service
systemctl daemon-reload
systemctl start blacklist
systemctl enable blacklist
sed -i "s/SCRIPT_PATH/$sedsafedir/g" $dir/cronjob
cp $dir/cronjob /etc/cron.d/blacklist
git -C $dir restore cronjob
systemctl restart cron
sed -r -i.orig 's/#?DNSStubListener=yes/DNSStubListener=no/g' /etc/systemd/resolved.conf
sh -c 'rm /etc/resolv.conf && ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf'
systemctl restart systemd-resolved
$dir/install.sh 
printf "rebooting the system...\n"
sleep 5
reboot
