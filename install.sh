#!/bin/bash
ipaddr=$(hostname -I | awk '{print $1}')
dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
$dir/install-pihole.sh
cat $dir/configuredb.sh | docker exec -i pihole bash -s $ipaddr
$dir/update.sh
printf "\nDUNZA!\n\n"
