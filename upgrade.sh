#/!bin/bash
#Download new docker container before purging the old one
dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
docker pull pihole/pihole
docker rm -f pihole
systemctl stop blacklist
$dir/install.sh
